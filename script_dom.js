let contador = 0
let soma = 0
var numero = 0


// função pra exibir as notas


function exibir_notas() {
    let paragrafo = document.createElement("p")
    let input = document.querySelector(".s-nota")
    paragrafo.innerText = input.value
    if (paragrafo.innerText.trim() === "" || parseFloat(paragrafo.innerText) < 0 || isNaN(paragrafo.innerText) == true) {
        return alert("A nota digitada é invalida, por favor, digite uma nota válida.");
    }

    let numero = parseFloat(paragrafo.innerText)
    
    if (numero > 10) {
        return alert("Digite uma nota abaixo de 11!")
    }

    else {
        let secaovar = document.querySelector(".quadro")
        let caixa = document.createElement("p")
        secaovar.append(caixa) 
        caixa.append(paragrafo)
        soma = soma + numero
        contador++
        paragrafo.innerText = "A nota " + contador + " foi " + numero + "."
    }
}


// função pra começar

function começar (){
    if (numero < 11) {
        while (true) {
            exibir_notas();
                break
        }
    }
}



// função pra calcular e printar a média

function calcularMedia() {
    var media = soma / contador
    let m = document.querySelector(".mostrar_media")
    m.innerText = "A média é: " + media.toFixed(2)
}

let adicionar = document.querySelector(".adicionar");
adicionar.addEventListener("click", ()=>começar())

let calcular = document.querySelector(".calcular_media");
calcular.addEventListener("click", ()=>calcularMedia())